jQuery(document).ready(function($){

    $(".menu-collapsed").click(function() {
        $(this).toggleClass("menu-expanded");
    });

    var nav_desktop = $('.main_nav').find('.iv_nav_desk').html();
    $('.iv_nav_xs').find('nav').html(nav_desktop);

    $('.testimonials_list').slick({
        dots: false,
        autoplay:true,
        autoplaySpeed:3000,
        arrows: false,
        variableWidth: true,
        centerPadding: '20px',
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    centerMode: true
                }
            }
        ]
    });

    $('.btn_up').on('click', function(e) {
        e.preventDefault();$('html, body').animate({scrollTop:0}, '300');
    });
});
